
(function ( $ ) {
/*
In order to use this plugin you have to include jquery
	imagePlayer extends jQuery.
	@param Object arg
*/
$.fn.imagePlayer = function(arg) {  
    // get the current Id of the div
    var currentId = this.attr('id');
       

	//store the id name in the arg object or get it from there if is passed 
    if(currentId == undefined){
        if(arg.id != undefined){
          currentId = arg.id;
        }
    }else{
      arg.id = currentId;
    }
    
    if(arg.scriptStarted == undefined){
      arg.scriptStarted = false;
      $('#'+currentId).hide();  
    }
      this.play = _play ;
      this.nextImage = _nextImage ; 

  
        
           
		 // get the filename without extension  in filename var and file extension in the extension variable. 
        var filename = $("#"+arg.id+" img").attr('src').split('/');

       
        filename = filename[filename.length-1]; 
        var extension = filename.split('.');
        filename =extension[0]; 
        extension = '.'+extension[extension.length-1];
        
        
        var nrZeros = 0;

        for( var i=0; i < filename.length; i++){
        
            if(filename.substr(i,1)==0)  nrZeros++; 
            else break;
           
        }     
		// you can pass  the extension of the image attribute 
      if(arg.extension == undefined){
        arg.extension  =extension;
      }
	  // if you don't pass the path attribute it will autodetect the image path
      if(arg.path == undefined){
             var path = $("#"+arg.id+" img").attr('src').split('/');
              path[path.length-1]= '';
              path = path.join('/');
              arg.path  =path;
      }      
	  // filename of the image
      if(arg.filename == undefined){
        arg.filename  =filename;
      }  
                     
		// a image can be somehting like 1.jpg or 01.jpg the zeroCount it reffers to how many zeros it have the file begins			 
      if(arg.zeroCount == undefined){
        arg.zeroCount  =nrZeros;
      }
    
	// define the first image: 
	//ex: 3
      if(arg.startFrom == undefined){
        arg.startFrom = 0 ;
      }
		// what image is rendered now 	  
      if(arg.currentCursor == undefined){
        arg.currentCursor = arg.startFrom ;
      }             
	  // if the animation is playing
      if(arg.play == undefined){
        arg.play=  false;
     
      } 
	  // the direction of the animation
	  // 0 - to right
	  // 1 - to left
      if(arg.direction == undefined){
        arg.direction =  0;
      } 
	  // the last image
      if(arg.maxImages == undefined){
        arg.maxImages =  24;
      }      
	  // time betewen displaying 2 images
      if(arg.animationTime == undefined){
        arg.animationTime =  1000;
      }         
	  // if you set the autoPlay to true it will  play the next image
       if(arg.autoPlay == undefined){
        arg.autoPlay =  false;
      }  
	  // TimeOut object
      if(arg.t == undefined){
        arg.t =  false;
      }      
 
if(arg.scriptStarted == false){
      //preload images
      for (var i = arg.startFrom; i <= arg.maxImages; i++) {                
                var imageObj = new Image();            
                                  
                imageObj.src = arg.path+getZerosNr(i.toString(),filename.length)+arg.extension;
                                 
                imageObj.onload= (function(i){

                            return function(){
                                    if( i == arg.maxImages){
                                        startTheScript();
                                      }                                      
                            }
                        })(i);
                        }
                     
}else{
 startTheScript();
}

// get the exact nr of 0 needed
 function getZerosNr(i,srcLength){
      var a = '';
      
      for(var k=0; k< parseInt(srcLength)- i.length;k++) a +='0';
      
     return a+i;
 }
 function startTheScript(){		

 if(arg.scriptStarted == false) $('.flstpreload').hide();    
    arg.scriptStarted =true;
	  // when the user moves the mouse the slider 
      updateSlider();
		// if already exist a click event
       $("#toleft"). button().unbind('click');
      //create a left animation button using jquery ui and add onclick event
	  $('#toleft'). button()         
        .click(function( event ) {

            clearTimeout(arg.t);// clear any  TimeOut event started
            event.preventDefault();
           arg.autoPlay = true;//set the autoplay to true
            arg.play = true; 
           arg.direction =  1;
           _nextImage();
            updateSlider();
            _play();
              
        });
       $("#toright"). button().unbind('click');
	   //create a right animation button using jquery ui and add onclick event
      $('#toright'). button()
        .click(function( event ) {
            clearTimeout(arg.t);
            event.preventDefault();
           arg.autoPlay = true;
            arg.play = true;
           arg.direction =  0;
           _nextImage();
            updateSlider();
            _play();
              
        });     
        
      if(arg.scriptStarted ) $('#'+currentId).fadeIn();   
     if(arg.play ) _play();       
  }        
      // update the slider
      function updateSlider (){       
   $( "#slider").slider({ min: arg.startFrom,max: arg.maxImages,value:arg.currentCursor,slide:change });
      }
	  // when the slider is moved by the user update the animation
      function change(e,ui){
                  
             arg.play = false;
             arg.currentCursor = ui.value;
             _nextImage();
             clearTimeout(arg.t);

      }


	  // compose the filename without extension adding as many 0 as it is necesarly
 function getIndex(t){

     if(arg.zeroCount > 0){
            var plusZero='';
            
           if(t < 10){
                
                for(var i=0;i<arg.zeroCount;i++) plusZero += '0';
                
           }else{
              for(var i=0;i<arg.zeroCount-1;i++) plusZero += '0';
           }
           t = plusZero+t;
      }
      return t;
     
 }   

  function _play(){
       

 if(arg.scriptStarted == true) {
  

               //compose the full image path with name 
  var img_path = (arg.currentCursor,arg.path+getIndex(arg.currentCursor)+ arg.extension);
 
              $("#"+arg.id+" img").attr('src',img_path);
  if(arg.play  ){
		// if animation is playable then add clear any Timeout event started and start another one
      clearTimeout(arg.t);                   
      arg.t =  setTimeout("$('#"+arg.id+"').imagePlayer("+JSON.stringify(arg)+").nextImage()",arg.animationTime);
        
      }  
    }
  } 
  // increment in the direction set by user with 1
  function increment(){

         if(arg.direction ==0){
             arg.currentCursor++
             if(arg.loop == true && arg.currentCursor > arg.maxImages){
                arg.currentCursor =arg.startFrom;
             }else if(arg.currentCursor > arg.maxImages){
             //step back 
             arg.currentCursor-- ;
             arg.play = false;   
              return false;             
             }
         }else{
            arg.currentCursor--;
              if(arg.loop == true && arg.currentCursor < arg.startFrom){
                arg.currentCursor =arg.maxImages;
             }else if(arg.currentCursor < arg.startFrom){
             //step back 
             arg.currentCursor++ ;
             arg.play = false;      
             return false;
             
             }
         }
         return true;

  }
  //get the next image
  function _nextImage(){

          if(increment()){ 
           
            _play();
           
          }  

         
  }       
                           
return this;
};
}( jQuery ));