$(document).ready(function(){
    $( "#tabs_top" ).tabs({  
      //collapsible: true
      /*beforeActivate: function (event, ui) {
        var old = ui.oldPanel.attr('id');
        var oldid = '#' + old;
        $(oldid).children().remove();
        console.log('Anterior ' + old);
      },
      activate: function (event, ui) {
        var newP = ui.newPanel.attr('id');
        var newPid = '#' + newP;
        console.log('Active ' + newPid);
      },*/
    });
    $( "#tabs_bot" ).tabs({
      //collapsible: true
    });
    var icons = {
      header: "plus",
      activeHeader: "minus"
    };
    $( "#accordion_alert" ).accordion({
      heightStyle: "content",
      collapsible: true,
      icons: icons
    });
    heightTable();
    /*var $gallery = $('.gallery').flickity({
      autoPlay: 300,
      pageDots: false
    });*/
    
});

var sup1 = new RubbableGif({ gif: document.getElementById('nubo'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
sup1.load();

var sup2 = new RubbableGif({ gif: document.getElementById('preci'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
sup2.load();

var sup3 = new RubbableGif({ gif: document.getElementById('vientos'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
sup3.load();

var bot1 = new RubbableGif({ gif: document.getElementById('viento1'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
bot1.load();

var bot2 = new RubbableGif({ gif: document.getElementById('temp'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
bot2.load();

var bot3 = new RubbableGif({ gif: document.getElementById('preci1'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
bot3.load();

var bot4 = new RubbableGif({ gif: document.getElementById('hum'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
bot4.load();

var bot5 = new RubbableGif({ gif: document.getElementById('atm'), max_width: '500', progressbar_background_color: '#FFF', progressbar_foreground_color: '#000', progressbar_height: '2' } );
bot5.load();

$(window).resize(function(){
    heightTable();
});
/*$(window).load(function(){
  $('#modalAlert').modal('show');
});*/

var toggleSlide = function(){
  $("#tabs_top ul li.ui-state-active").removeClass().next().add("#tabs_top ul li:first").last().addClass("ui-state-active").children().trigger('click');
  $("#tabs_bot ul li.ui-state-active").removeClass().next().add("#tabs_bot ul li:first").last().addClass("ui-state-active").children().trigger('click');
}
setInterval(toggleSlide, 15000);

/*$( "#nubosidad-top" ).click(function() {
    $( ".result-1" ).load( "ajax/nubosidad.html" );
});
$( "#precipitacion-top" ).click(function() {
    $( ".result-2" ).load( "ajax/precipitacion.html" );
});
$( "#vientos-top" ).click(function() {
    $( ".result-3" ).load( "ajax/vientos.html" );
});

$( "#tab-bot-1" ).click(function() {
    $( ".result-2-1" ).load( "ajax/viento2.html" );
});
$( "#tab-bot-2" ).click(function() {
    $( ".result-2-2" ).load( "ajax/temperatura.html" );
});
$( "#tab-bot-3" ).click(function() {
    $( ".result-2-3" ).load( "ajax/precipitacion2.html" );
});
$( "#tab-bot-4" ).click(function() {
    $( ".result-2-4" ).load( "ajax/humedad.html" );
});
$( "#tab-bot-5" ).click(function() {
    $( ".result-2-5" ).load( "ajax/presion.html" );
});*/

function heightTable(){
    var div = document.querySelector('.prognostic-box ul'),
    space = innerHeight - div.offsetTop - div.offsetHeight - 53;
    //console.log(space);
    if(space < 0){
        space = Math.abs(space)
    //console.log(space);
    }
    $(".table-prognostic").css('height',space + 'px');
}